import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PesquisaFilmesPage } from './pesquisa-filmes';

@NgModule({
  declarations: [
    PesquisaFilmesPage,
  ],
  imports: [
    IonicPageModule.forChild(PesquisaFilmesPage),
  ],
})
export class PesquisaFilmesPageModule {}
