import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MoviesProvider } from '../../providers/movies/movies';

@IonicPage()
@Component({
  selector: 'page-info-filmes',
  templateUrl: 'info-filmes.html',
  providers:[
    MoviesProvider
  ]
})
export class InfoFilmesPage {
  public filme;
  public filmeid;
  constructor(public navCtrl: NavController, public navParams: NavParams, public movieProvider:MoviesProvider) {
  }

  ionViewDidLoad() {
    this.filmeid = this.navParams.get("id");
    this.movieProvider.getLatestMovieDetalhes(this.filmeid).subscribe(data=>{
        let retorno = (data as any)._body;
        this.filme=JSON.parse(retorno);
    },error=>{
      console.log(error);
    })
  }

}
