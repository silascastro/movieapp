import { FormBuilder, FormGroup , Validators} from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { LoginProvider } from '../../providers/login/login';
import { HomePage } from '../home/home';



@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {
  form: FormGroup;
  constructor(public navCtrl: NavController,private makeLogin: LoginProvider,fb: FormBuilder, private toastCtrl: ToastController) {
    this.form = fb.group({
      email: ['', Validators.compose([Validators.required])],
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    //console.log(this.username);
  }

  submit(){
    let r = this.makeLogin.cadastrar(this.form.controls['email'].value, this.form.controls['password'].value);
    r.then((Response) => {
      Response.user.updateProfile({
        displayName: this.form.controls['username'].value,
        photoURL: ""
      });

      this.presentToast('Cadastro realizado com sucesso!');
      this.navCtrl.setRoot(HomePage);
      console.log(Response);
    }).catch((error) =>{
      if(error.code == 'auth/network-request-failed'){
        this.presentToast('network-request-failed');

      }
      if (error.code == 'auth/email-already-in-use'){
        this.presentToast('Esse email já é usado por outro usuário');
      }
    })

  }

  presentToast(title: string){
    let toast = this.toastCtrl.create({
      message: title,
      duration: 3000 
    });
    toast.present();
  }

  

}
