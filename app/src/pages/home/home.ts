import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController} from 'ionic-angular';
import { InfoFilmesPage } from '../info-filmes/info-filmes';
import { MoviesProvider } from '../../providers/movies/movies';
import { LoginProvider } from '../../providers/login/login';

@IonicPage()
@Component({

  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[
    MoviesProvider
  ]
})

export class HomePage {
  username: any;
  constructor(public navCtrl: NavController,private movieProvider:MoviesProvider,public loadingCtrl: LoadingController, public auth: LoginProvider) {
    //this.username = auth.user();
    //this.username = 'Usuário';
    this.username = this.auth.username();
  }

  //declarando variavel para ser usada no openLoading
  public loader;

  public refresher;

  public isRefreshing:boolean = false;

  //Loading de abertura para aviso de carregamento
  openLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando filmes",
    });
    this.loader.present();
  }

  //Função responsável por fechar o loading
  closeLoading(){
    this.loader.dismiss();
  }

  public lista_filmes = new Array<any>();


  ionViewDidLoad() {
    this.carregarFilmes();
    
  }

  carregarFilmes(){
        //Chamando fução que carrega lista de filmes
        this.openLoading();

        console.log('ionViewDidLoad HomePage');

        this.movieProvider.getLatestMovies().subscribe(
          data=>{
            console.log("API FUNCIONANDO COM SUCESSO !! :)")
            const response = (data as any);
            const obj_return = JSON.parse(response._body);
            this.lista_filmes = obj_return.results;

            console.log(obj_return);
            this.closeLoading();

            if(this.isRefreshing){
              this.refresher.complete();
              this.isRefreshing = false;
            }

          },error=>{
            console.log(error);
            this.closeLoading();
            if(this.isRefreshing){
              this.refresher.complete();
            }
          }
        )
  }

  //Função que retorna a abertura de uma página passando como valor de retorno a pagina
  GoToPage(page){
    if(page=='LoginPage'){

      const l = this.auth.user().subscribe(user =>{
        if(user){
          console.log(user);
          l.unsubscribe();
        }else{
          console.log(user);
          l.unsubscribe();
        }
      });

      this.auth.logout();
      const r = this.auth.user().subscribe(user =>{
        if(user){
          console.log(user);
          r.unsubscribe();
        }else{
          console.log(user);
          r.unsubscribe();
        }
      });
      this.navCtrl.setRoot(page);

    }else{
      let r =this.auth.user().subscribe(user => {
        if(user){
          console.log(user);
          r.unsubscribe(); 
        }else{
          console.log(user);
          r.unsubscribe();
        }
      });
      this.navCtrl.push(page);
    }
  }

  filmeDetalhes(filme){
    console.log(filme);
    console.log("funcionando");
    this.navCtrl.push(InfoFilmesPage, {id: filme.id});
  }

  doRefresh(refresher) {

    this.refresher = refresher;
    this.isRefreshing = true;
    this.carregarFilmes();

    console.log("Refresh efeituado com sucesso");

  }

  filmPage(filme:any){

    this.navCtrl.push(InfoFilmesPage,{
      nome : filme.nome,
      genero : filme.genero,
      imagem : filme.imagem,
      descricao : filme.descricao,
      lancamento: filme.lancamento,
      duracao : filme.duracao,
      elenco : filme.elenco,
      diretor: filme.diretor
    });
  }
}
