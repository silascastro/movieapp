import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CadastroPage } from '../cadastro/cadastro';
import { LoginProvider } from '../../providers/login/login';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  //email:string;
  //password:string;
  public form: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,private auth: LoginProvider, private toastCtrl: ToastController, fb: FormBuilder) {
    this.form = fb.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() { 
    console.log('loginPage loading...');
  }

  submit(){
    //this.navCtrl.push(page,{user: this.email}) //passando parametros para a página home
    let r = this.auth.login(this.form.controls['email'].value,this.form.controls['password'].value);

    r.then((response) => {
      //let user = response.user.displayName;
      this.presentToast('Usuário autenticado com sucesso');
      this.navCtrl.setRoot(HomePage);
      console.log(response);
    })
    .catch((error) => {

        
        if(error.code == 'auth/wrong-password'){
          this.presentToast('Senha incorreta');
          console.log(error);
        }
        if(error.code == 'auth/invalid-email'){
          this.presentToast('Email incorreto');
          console.log(error);
        }
        
    })
  }




  GoToCadastro(){
    this.navCtrl.push(CadastroPage);
  }

  presentToast(title: string){
    let toast = this.toastCtrl.create({
      message: title,
      duration: 3000 
    });
    toast.present();
  }
}
