import { Injectable } from '@angular/core';

let config_key_name = 'config'

@Injectable()
export class ConfigProvider {
  
  private config = {
    showSlide: false,
    username: ""
  }

  constructor() {} 


  getConfigData(): any{
    return localStorage.getItem(config_key_name);
  }

  setConfigData(showSlide?: boolean, username?: string){
    this.config = {
      showSlide: false,
      username: ""
    };

    if(showSlide){
      this.config.showSlide = showSlide;
    }

    if(username){
      this.config.username = username;
    }

    localStorage.setItem(config_key_name, JSON.stringify(this.config));
  }
}
