import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';



@Injectable()
export class LoginProvider {

  constructor(private afAuth: AngularFireAuth) {
    
    console.log('Hello LoginProvider Provider');
    //console.log(this.afAuth.auth.currentUser);
    }


  cadastrar(email: string, password: string){
    return this.afAuth.auth.createUserWithEmailAndPassword(email,password);
  }

  login(email: string, password: string){
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  logout(){
    return this.afAuth.auth.signOut();
  }
  username(){
    return this.afAuth.auth.currentUser.displayName;
  }
  user(){
    return this.afAuth.authState;
  }

}
