import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class MoviesProvider {
  //Link da API
  private baseApiPath = "https://api.themoviedb.org/3";
  constructor(public http: Http) {
    console.log('Hello MoviesProvider Provider');
  }

  //busca de informações da API
  getLatestMovies(){

    //concatenação do link para requisição + api key
    return this.http.get(this.baseApiPath + "/movie/popular?api_key=607cf92e63e2529484c42b68e8b6267c");
  }

  getApiKey(){
    return '607cf92e63e2529484c42b68e8b6267c';
  }

  getLatestMovieDetalhes(filmeid){

    //concatenação do link para requisição + api key
    return this.http.get(this.baseApiPath + `/movie/${filmeid}?api_key=` + this.getApiKey());
  }

}
