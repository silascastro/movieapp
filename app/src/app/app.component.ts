import { HomePage } from './../pages/home/home';
import { ConfigProvider } from './../providers/config/config';
import { Component } from '@angular/core';
import { Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SlidesPage } from '../pages/slides/slides';
import { LoginPage } from '../pages/login/login';
import { LoginProvider } from '../providers/login/login';

@Component({
  templateUrl: 'app.html',
  providers: [
    ConfigProvider
  ]
})

export class MyApp {

  
  rootPage:any;
  config: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,configProv: ConfigProvider, auth: LoginProvider) {
    
   
    platform.ready().then(() => {

      this.config = configProv.getConfigData();
      if(this.config == null){
        this.rootPage = SlidesPage;
        configProv.setConfigData(false);
        console.log(this.config);
      }else{
        let r = auth.user().subscribe( user =>{
          if(user){
            this.rootPage = HomePage;
            r.unsubscribe();
          }else{
            this.rootPage = LoginPage;
            r.unsubscribe();
            console.log(this.config);
          }
        })

      }
      //console.log(this.config == null);
     // console.log(this.config);
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
