import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { SlidesPageModule } from '../pages/slides/slides.module';
import { HomePageModule } from '../pages/home/home.module';
import { PerfilPageModule } from '../pages/perfil/perfil.module';
import { CategoriasPageModule } from '../pages/categorias/categorias.module';
import { ConfiguracoesPageModule } from '../pages/configuracoes/configuracoes.module';
import { ConquistasPageModule } from '../pages/conquistas/conquistas.module';
import { FeedbackPageModule } from '../pages/feedback/feedback.module';
import { PesquisaFilmesPageModule } from '../pages/pesquisa-filmes/pesquisa-filmes.module';
import { LoginPageModule } from '../pages/login/login.module';
import { HttpModule } from '@angular/http';
import { MoviesProvider } from '../providers/movies/movies';
import { InfoFilmesPageModule } from '../pages/info-filmes/info-filmes.module';
import { AngularFireModule } from '@angular/fire';
import { LoginProvider } from '../providers/login/login';
import { CadastroPageModule } from '../pages/cadastro/cadastro.module';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    MyApp,

  ],

  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SlidesPageModule,
    CadastroPageModule,
    HomePageModule,
    PerfilPageModule,
    CategoriasPageModule,
    ConfiguracoesPageModule,
    ConquistasPageModule,
    FeedbackPageModule,
    PesquisaFilmesPageModule,
    LoginPageModule,
    HttpModule,
    InfoFilmesPageModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyDmf7ptAVX-G0Sv4M48MzZ8vKY8sU7NR6M",
      authDomain: "moviemanicdatabase.firebaseapp.com",
      databaseURL: "https://moviemanicdatabase.firebaseio.com",
      projectId: "moviemanicdatabase",
      storageBucket: "moviemanicdatabase.appspot.com",
      messagingSenderId: "663990645158"
    }),
    HttpModule,
    HttpClientModule
    
  ],

  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
 
  providers: [
    AngularFireAuth,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MoviesProvider,
    LoginProvider
  ]
})

export class AppModule {}
